| Title                        | Date       | Name             | Category             | Amount | Balance | Memo    |
| ---------------------------- | ---------- | ---------------- | -------------------- | ------ | ------- | --- |
| 2022/2022-01-01-Transaction1 | 2022-01-01 | Grocery Store    | Groceries            | -50    | 950     | Fruits and Veggies    |
| 2022/2022-01-02-Transaction2 | 2022-01-02 | Electric Company | Utilities            | -100   | 850     |     |
| 2022/2022-01-03-Transaction3 | 2022-01-03 | Employer         | Salary               | 1000   | 1850    |     |
| 2022/2022-01-04-Transaction4 | 2022-01-04 | Restaurant       | Dining               | -70    | 1780    |     |
| 2023/2023-05-13-Transaction5 | 2023-05-13 | Walmart          | Electronics | -1000  | 780     | Purchase a Azus 123 Laptop    |
| 2023/2023-06-24-Transaction6 | 2023-06-24 | BestBuy          | Electronics               | -170   | 610     | Monitor    |
See also: [[Sample CSV data]]
