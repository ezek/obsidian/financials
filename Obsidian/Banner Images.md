
> [!Attention] This is a list of available banner images for this vault.

#todo this note should use a dataview query to get and display images inside the banners folder.


![[banner-statement-balance.png]]


![[banner-ipad-macbook-chart-1.png]]


![[banner-a-stack-of-100-dollars.png]]


![[banner-ipad-macbook-chart-2.png]]


![[banner-digital-graph.png]]

References:
```dataview
List
from "Financials/Obsidian"
```
Need to ask AI what types of templates should we create.

Need to configure charts and stuff.
Can we have a double entry display? Like money?