
```csv
Title,Name,Date,Category,Amount,Balance,Memo
2022/2022-01-01-Transaction1,Grocery Store,2022-01-01,Groceries,-50,950,Fruits and Veggies
2022/2022-01-02-Transaction2,Electric Company,2022-01-02,Utilities,-100,850,
2022/2022-01-03-Transaction3,Employer,2022-01-03,Salary,1000,1850,
2022/2022-01-04-Transaction4,Restaurant,2022-01-04,Dining,-70,1780,
2023/2023-05-13-Transaction5,Walmart,2023-05-13,Electronics,-1000,780,Purchase a Azus 123 Laptop
2023/2023-06-24-Transaction6,BestBuy,2023-06-24,Electronics,-170,610,Monitor
2023/2023-07-01-Transaction7,Employer,2023-07-01,Salary,1000,1610,
2023/2023-07-02-Transaction8,Restaurant,2023-07-02,Dining,-70,1540,
2023/2023-07-03-Transaction9,Electric Company,2023-07-03,Utilities,-100,1440,
2023/2023-07-04-Transaction10,Grocery Store,2023-07-04,Groceries,-50,1390,
```
