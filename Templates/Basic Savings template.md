---
tags: 
banner: "![[banner-a-stack-of-100-dollars.png]]"
banner_x: 0.12316
---

# {{Name}}

***


| Date | Name | Category | Amount | Balance |
| ---- | ---- | ---- | ---- | ---- |
| {{Date}} | {{Name}} | {{Category}} | {{Amount}} | {{Balance}} |

*** 

> [!Abstract] {{Date}}
> This is my {{Name}} transaction for {{Amount}}.
> 
> {{Memo}}

References: #Transaction/Income