---
tags:
  - {{Category}}
banner: "![[1.png]]"
---

# {{Name}}

***

| Date     | Name     | Category     | Amount     | Balance     |
| -------- | -------- | ------------ | ---------- | ----------- |
| {{Date}} | {{Name}} | {{Category}} | {{Amount}} | {{Balance}} |
 
***

> [!Abstract] {{Date}}
> This is my {{Name}} transaction for {{Amount}}.
> 
> {{Memo}}

References: #Transaction/Expense